import React from 'react';
import './page-error.scss';

const Page404 = () => {
    return (
        <>
        <h1 className='error-page'>
            404 - <span> page not found! </span>
        </h1>
        </>
    );
};

export default Page404;