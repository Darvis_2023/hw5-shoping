import React, {useState} from 'react';

const Head = () => {

    return (
        <header className="container header-wrapper">
            <a className="header__company-logo" href="/">Top-mobile.com</a>
            <a className="header__productList" href="/productList"><img width={30} src="https://cdn0.iconfinder.com/data/icons/typicons-2/24/home-512.png" alt="" /></a>
            <a className="header__favorites" href="/favorites"><img width={25} src="https://cdn2.iconfinder.com/data/icons/ui-22-start-ecommerce-pack/12/ecommerce_like-512.png" alt="" /></a>
            <a className="header__cart" href="/cart"><img width={30} src="https://cdn0.iconfinder.com/data/icons/typicons-2/24/shopping-cart-512.png" alt="" /></a>

        </header>
    );
};

export default Head;